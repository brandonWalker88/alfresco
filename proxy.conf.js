module.exports = {
  "/auth/admin/realms/myrealm": {
    "target": "http://{YOUR IP ADDRESS}:80",
    "secure": false,
    "pathRewrite": {
      "^/auth/admin/realms/myrealm": ""
    },
    "changeOrigin": true,
    "logLevel": "debug"
  },
  "/auth/realms/myrealm": {
    "target": "http://{YOUR IP ADDRESS}:80",
    "secure": false,
    "pathRewrite": {
      "^/auth/realms/myrealm": ""
    },
    "changeOrigin": true,
    "logLevel": "debug"
  },
  "/": {
    "target": "http://{YOUR IP ADDRESS}:80",
    "secure": false,
    "changeOrigin": true,
    "logLevel": "debug"
  },
  "/alfresco": {
    "target": "http://{YOUR IP ADDRESS}:80",
    "secure": false,
    "changeOrigin": true
  }
}
