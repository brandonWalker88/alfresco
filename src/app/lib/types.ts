export type ComponentSelectionMode = 'single' | 'multiple';
