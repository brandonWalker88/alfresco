import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation, OnDestroy, ViewChild } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import moment from 'moment-es6';
import { Moment } from 'moment';
import { Observable, Subject } from 'rxjs';
import { FormBuilder, AbstractControl, Validators, FormGroup, FormControl } from '@angular/forms';
import {
    MOMENT_DATE_FORMATS, MomentDateAdapter,
    LogService,
    UserPreferencesService,
    IdentityUserService,
    IdentityUserModel,
    UserPreferenceValues
} from '@alfresco/adf-core';
import { IgPeopleComponent } from '../peoples-component/ig-people-component';
import { IgGroupComponent } from '../group-component/ig-group.component';
import { IgTaskService } from '../../services/ig-task.service';
import { IgStartTaskRequestModel } from '../../models/ig-start-task-request.model';
import { takeUntil } from 'rxjs/operators';
import { IgTaskPriorityOption } from '../../models/ig-task.model';

@Component({
    selector: 'app-ig-start-task',
    templateUrl: './ig-start-task.component.html',
    styleUrls: ['./ig-start-task.component.scss'],
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter },
        { provide: MAT_DATE_FORMATS, useValue: MOMENT_DATE_FORMATS }],
    encapsulation: ViewEncapsulation.None
})

export class IgStartTaskComponent implements OnInit, OnDestroy {

    static MAX_NAME_LENGTH = 255;

    public DATE_FORMAT: string = 'DD/MM/YYYY';

    /** (required) Name of the app. */
    @Input()
    appName: string = '';

    /** Maximum length of the task name. */
    @Input()
    maxNameLength: number = IgStartTaskComponent.MAX_NAME_LENGTH;

    /** Name of the task. */
    @Input()
    name: string = '';

    /** Emitted when the task is successfully created. */
    @Output()
    success: EventEmitter<any> = new EventEmitter<any>();

    /** Emitted when the cancel button is clicked by the user. */
    @Output()
    cancel: EventEmitter<void> = new EventEmitter<void>();

    /** Emitted when an error occurs. */
    @Output()
    error: EventEmitter<any> = new EventEmitter<any>();

    @ViewChild('peopleInput')
    assignee: IgPeopleComponent;

    @ViewChild('groupInput')
    candidateGroups: IgGroupComponent;

    users$: Observable<any[]>;

    taskId: string;

    dueDate: Date;

    dueDate2: Date;

    submitted = false;

    assigneeName: string;

    dateError: boolean;

    taskForm: FormGroup;

    currentUser: IdentityUserModel;

    formKey: string;

    priorities: IgTaskPriorityOption[];

    private assigneeForm: AbstractControl = new FormControl('');
    private groupForm: AbstractControl = new FormControl('');
    private onDestroy$ = new Subject<boolean>();

    constructor(private taskService: IgTaskService,
                private dateAdapter: DateAdapter<Moment>,
                private userPreferencesService: UserPreferencesService,
                private formBuilder: FormBuilder,
                private identityUserService: IdentityUserService,
                private logService: LogService) {
    }

    ngOnInit() {
        this.userPreferencesService
            .select(UserPreferenceValues.Locale)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe(locale => this.dateAdapter.setLocale(locale));
        this.loadCurrentUser();
        this.buildForm();
        this.loadDefaultPriorities();
    }

    ngOnDestroy() {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    }

    buildForm() {
        this.taskForm = this.formBuilder.group({
            name: new FormControl(this.name, [Validators.required, Validators.maxLength(this.getMaxNameLength()), this.whitespaceValidator]),
            priority: new FormControl(''),
            description: new FormControl('', [this.whitespaceValidator]),
            notes: new FormControl('', [this.whitespaceValidator]),
            formKey: new FormControl()
        });
    }

    private getMaxNameLength(): number {
        return this.maxNameLength > IgStartTaskComponent.MAX_NAME_LENGTH ?
            IgStartTaskComponent.MAX_NAME_LENGTH : this.maxNameLength;
    }

    private loadCurrentUser() {
        this.currentUser = this.identityUserService.getCurrentUserInfo();
        this.assigneeName = this.currentUser.username;
    }

    private loadDefaultPriorities() {
        this.priorities = this.taskService.priorities;
    }

    public saveTask() {
        this.submitted = true;
        const newTask = Object.assign(this.taskForm.value);
        newTask.dueDate = this.dueDate;
        newTask.dueDate2 = this.dueDate2;
        newTask.assignee = this.assigneeName;
        newTask.formKey = this.formKey;

        this.createNewTask(new IgStartTaskRequestModel(newTask));
    }

    private createNewTask(newTask: IgStartTaskRequestModel) {
        this.taskService.createNewTask(newTask, this.appName)
            .subscribe(
                (res: any) => {
                    this.submitted = false;
                    this.success.emit(res);
                },
                (err) => {
                    this.submitted = false;
                    this.error.emit(err);
                    this.logService.error('An error occurred while creating new task');
                });
    }

    public onCancel() {
        this.cancel.emit();
    }

    onDateChanged(newDateValue) {
        this.dateError = false;

        if (newDateValue) {
            const momentDate = moment(newDateValue, this.DATE_FORMAT, true);
            if (!momentDate.isValid()) {
                this.dateError = true;
            }
        }
    }

    onDateChanged2(newDateValue) {
        this.dateError = false;

        if (newDateValue) {
            const momentDate = moment(newDateValue, this.DATE_FORMAT, true);
            if (!momentDate.isValid()) {
                this.dateError = true;
            }
        }
    }

    onAssigneeSelect(assignee: IdentityUserModel) {
        this.assigneeName = assignee ? assignee.username : '';
    }

    onAssigneeRemove() {
        this.assigneeName = '';
    }

    canStartTask(): boolean {
        return !(this.dateError ||
            !this.taskForm.valid ||
            this.submitted ||
            this.assignee.hasError() );
    }

    public whitespaceValidator(control: FormControl) {
        const isWhitespace = (control.value || '').trim().length === 0;
        const isValid = control.value.length === 0 || !isWhitespace;
        return isValid ? null : { 'whitespace': true };
    }

    get nameController(): AbstractControl {
        return this.taskForm.get('name');
    }

    get priorityController(): AbstractControl {
        return this.taskForm.get('priority');
    }

    get assigneeFormControl(): AbstractControl {
        return this.assigneeForm;
    }

    get candidateUserFormControl(): AbstractControl {
        return this.groupForm;
    }

    onFormSelect(formKey: string) {
        this.formKey = formKey || '';
    }
}
