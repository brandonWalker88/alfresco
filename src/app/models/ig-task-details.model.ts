export interface IgTaskDetailsModel {
    id?: string;
    name?: string;
    appName?: string;
    assignee?: string;
    appVersion?: number;
    createdDate?: Date;
    claimedDate?: Date;
    completedDate?: Date;
    formKey?: any;
    category?: any;
    description?: string;
    notes: string;
    dueDate?: Date;
    dueDate2?: Date;
    lastModified?: Date;
    lastModifiedTo?: Date;
    lastModifiedFrom?: Date;
    owner?: any;
    parentTaskId?: string;
    priority?: number;
    processDefinitionId?: string;
    processInstanceId?: string;
    status?: TaskStatus;
    standalone?: boolean;
    candidateUsers?: string[];
    managerOfCandidateGroup?: boolean;
    memberOfCandidateGroup?: boolean;
    memberOfCandidateUsers?: boolean;
    processDefinitionDeploymentId?: string;
}

export interface IgStartTaskResponseModel {
    entry: IgTaskDetailsModel;
}

export type TaskStatus = |
    'COMPLETED' |
    'CREATED' |
    'ASSIGNED' |
    'SUSPENDED' |
    'CANCELLED';
