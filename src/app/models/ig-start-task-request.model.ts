export class IgStartTaskRequestModel {

    name: string;
    description: string;
    assignee: string;
    priority: string;
    notes: string;
    dueDate: Date;
    dueDate2: Date;
    candidateUsers: string[];
    payloadType: string;
    formKey: string;

    constructor(obj?: any) {
        if (obj) {
            this.name = obj.name || null;
            this.description = obj.description || null;
            this.assignee = obj.assignee || null;
            this.priority = obj.priority || null;
            this.notes = obj.notes || null;
            this.dueDate = obj.dueDate || null;
            this.dueDate2 = obj.dueDate2 || null;
            this.candidateUsers = obj.candidateUsers || null;
            this.formKey = obj.formKey || null;
            this.payloadType = 'CreateTaskPayload';
        }
    }
}
