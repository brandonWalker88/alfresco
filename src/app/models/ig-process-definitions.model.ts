export class IgProcessDefinition {
    id: string;
    appName: string;
    key: string;
    formKey?: string;
    appVersion: number;
    version: number;
    name: string;

    constructor(obj?: any) {
        this.id = obj && obj.id || null;
        this.name = obj && obj.name || null;
        this.appName = obj && obj.appName || null;
        this.key = obj && obj.key || null;
        this.formKey = obj && obj.formKey || null;
        this.version = obj && obj.version || 0;
        this.appVersion = obj && obj.appVersion || 0;
    }
}
