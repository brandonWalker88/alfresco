export enum ClaimTaskEnum {
    claim = 'claim',
    unclaim = 'unclaim'
}

export interface IgTaskPriorityOption {
    label: string;
    key: string;
    value: string;
}

export const IG_DEFAULT_TASK_PRIORITIES: IgTaskPriorityOption[] = [
    { label: 'ADF_CLOUD_TASK_LIST.PROPERTIES.PRIORITY_VALUES.NONE', value: '0', key: '0' },
    { label: 'ADF_CLOUD_TASK_LIST.PROPERTIES.PRIORITY_VALUES.LOW', value: '1', key: '1' },
    { label: 'ADF_CLOUD_TASK_LIST.PROPERTIES.PRIORITY_VALUES.NORMAL', value: '2', key: '2' },
    { label: 'ADF_CLOUD_TASK_LIST.PROPERTIES.PRIORITY_VALUES.HIGH', value: '3', key: '3' }
];
