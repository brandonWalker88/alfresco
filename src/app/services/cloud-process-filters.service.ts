import { Injectable } from '@angular/core';
import { ProcessFilterCloudModel, ProcessFilterCloudService } from '@alfresco/adf-process-services-cloud';
import { AppConfigService } from '@alfresco/adf-core';

@Injectable({ providedIn: 'root' })
export class CloudProcessFiltersService {
    constructor(private appConfigService: AppConfigService, private processFilterCloudService: ProcessFilterCloudService) {
    }

    get filterProperties(): string[] {
        return this.appConfigService.get(
            'adf-edit-process-filter.filterProperties',
            ['status', 'sort', 'order', 'processName']
        );
    }

    get sortProperties(): string[] {
        return this.appConfigService.get(
            'adf-edit-process-filter.sortProperties',
            ['id', 'name', 'status', 'startDate']
        );
    }

    get actions(): string[] {
        return this.appConfigService.get(
            'adf-edit-process-filter.actions',
            ['save', 'saveAs', 'delete']
        );
    }

    readQueryParams(obj: Object): ProcessFilterCloudModel {
        return this.processFilterCloudService.readQueryParams(obj);
    }

    writeQueryParams(value: Object, appName?: string, id?: string): Object {
        return this.processFilterCloudService.writeQueryParams(value, this.filterProperties, appName, id);
    }
}
