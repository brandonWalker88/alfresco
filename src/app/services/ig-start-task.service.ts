import { Injectable } from '@angular/core';
import { AlfrescoApiService, AppConfigService } from '@alfresco/adf-core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IgStartTaskRequestModel } from '../models/ig-start-task-request.model';
import { IgTaskDetailsModel, IgStartTaskResponseModel } from '../models/ig-task-details.model';
import { IgBaseService } from '../services/ig-base.service';

@Injectable({ providedIn: 'root' })
export class IgStartTaskService extends IgBaseService {

    constructor(
        apiService: AlfrescoApiService,
        appConfigService: AppConfigService) {
        super(apiService, appConfigService);
    }

    /**
     * @deprecated in 3.5.0, use TaskCloudService instead.
     * Creates a new standalone task.
     * @param taskDetails Details of the task to create
     * @returns Details of the newly created task
     */
    createNewTask(taskDetails: IgTaskDetailsModel): Observable<IgTaskDetailsModel> {
        const url = `${this.getBasePath(taskDetails.appName)}/rb/v1/tasks`;
        const payload = JSON.stringify(new IgStartTaskRequestModel(taskDetails));

        return this.post<any, IgStartTaskResponseModel>(url, payload)
            .pipe(
                map(response => response.entry)
            );
    }
}
