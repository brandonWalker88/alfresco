import { Injectable } from '@angular/core';
import { AlfrescoApiService, LogService, AppConfigService, IdentityUserService, CardViewArrayItem, TranslationService } from '@alfresco/adf-core';
import { throwError, Observable, of, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { IgTaskDetailsModel, IgStartTaskResponseModel } from '../models/ig-task-details.model';
import { IgBaseService } from '../services/ig-base.service';
import { IgStartTaskRequestModel } from '../models/ig-start-task-request.model';
import { IgProcessDefinition } from '../models/ig-process-definitions.model';
import { IG_DEFAULT_TASK_PRIORITIES, IgTaskPriorityOption } from '../models/ig-task.model';

@Injectable({
    providedIn: 'root'
})
export class IgTaskService extends IgBaseService {

    static TASK_ASSIGNED_STATE = 'ASSIGNED';
    dataChangesDetected$ = new Subject();

    constructor(
        apiService: AlfrescoApiService,
        appConfigService: AppConfigService,
        private logService: LogService,
        private translateService: TranslationService,
        private identityUserService: IdentityUserService
    ) {
        super(apiService, appConfigService);
    }

    /**
     * Complete a task.
     * @param appName Name of the app
     * @param taskId ID of the task to complete
     * @returns Details of the task that was completed
     */
    completeTask(appName: string, taskId: string): Observable<IgTaskDetailsModel> {
        if ((appName || appName === '') && taskId) {
            const url = `${this.getBasePath(appName)}/rb/v1/tasks/${taskId}/complete`;
            const payload = { 'payloadType': 'CompleteTaskPayload' };

            return this.post<any, IgTaskDetailsModel>(url, payload);
        } else {
            this.logService.error('AppName and TaskId are mandatory for complete a task');
            return throwError('AppName/TaskId not configured');
        }
    }

    /**
     * Validate if a task can be completed.
     * @param taskDetails task details object
     * @returns Boolean value if the task can be completed
     */
    canCompleteTask(taskDetails: IgTaskDetailsModel): boolean {
        return taskDetails && taskDetails.status === IgTaskService.TASK_ASSIGNED_STATE && this.isAssignedToMe(taskDetails.assignee);
    }

    /**
     * Validate if a task is editable.
     * @param taskDetails task details object
     * @returns Boolean value if the task is editable
     */
    isTaskEditable(taskDetails: IgTaskDetailsModel): boolean {
        return taskDetails && taskDetails.status === IgTaskService.TASK_ASSIGNED_STATE && this.isAssignedToMe(taskDetails.assignee);
    }

    isAssigneePropertyClickable(taskDetails: IgTaskDetailsModel, candidateUsers: CardViewArrayItem[], candidateGroups: CardViewArrayItem[]): boolean {
        let isClickable = false;
        const states = [IgTaskService.TASK_ASSIGNED_STATE];
        if (candidateUsers?.length || candidateGroups?.length) {
            isClickable = states.includes(taskDetails.status);
        }
        return isClickable;
    }

    /**
     * Validate if a task can be claimed.
     * @param taskDetails task details object
     * @returns Boolean value if the task can be completed
     */
    canClaimTask(taskDetails: IgTaskDetailsModel): boolean {
        return taskDetails && taskDetails.status === 'CREATED';
    }

    /**
     * Validate if a task can be unclaimed.
     * @param taskDetails task details object
     * @returns Boolean value if the task can be completed
     */
    canUnclaimTask(taskDetails: IgTaskDetailsModel): boolean {
        const currentUser = this.identityUserService.getCurrentUserInfo().username;
        return taskDetails && taskDetails.status === IgTaskService.TASK_ASSIGNED_STATE && taskDetails.assignee === currentUser;
    }

    /**
     * Claims a task for an assignee.
     * @param appName Name of the app
     * @param taskId ID of the task to claim
     * @param assignee User to assign the task to
     * @returns Details of the claimed task
     */
    claimTask(appName: string, taskId: string, assignee: string): Observable<IgTaskDetailsModel> {
        if ((appName || appName === '') && taskId) {
            const queryUrl = `${this.getBasePath(appName)}/rb/v1/tasks/${taskId}/claim?assignee=${assignee}`;

            return this.post(queryUrl).pipe(
                map((res: any) => {
                    this.dataChangesDetected$.next();
                    return res.entry;
                })
            );
        } else {
            this.logService.error('AppName and TaskId are mandatory for querying a task');
            return throwError('AppName/TaskId not configured');
        }
    }

    /**
     * Un-claims a task.
     * @param appName Name of the app
     * @param taskId ID of the task to unclaim
     * @returns Details of the task that was unclaimed
     */
    unclaimTask(appName: string, taskId: string): Observable<IgTaskDetailsModel> {
        if ((appName || appName === '') && taskId) {
            const queryUrl = `${this.getBasePath(appName)}/rb/v1/tasks/${taskId}/release`;

            return this.post(queryUrl).pipe(
                map((res: any) => {
                    this.dataChangesDetected$.next();
                    return res.entry;
                })
            );
        } else {
            this.logService.error('AppName and TaskId are mandatory for querying a task');
            return throwError('AppName/TaskId not configured');
        }
    }

    /**
     * Gets details of a task.
     * @param appName Name of the app
     * @param taskId ID of the task whose details you want
     * @returns Task details
     */
    getTaskById(appName: string, taskId: string): Observable<IgTaskDetailsModel> {
        if ((appName || appName === '') && taskId) {
            const queryUrl = `${this.getBasePath(appName)}/query/v1/tasks/${taskId}`;

            return this.get(queryUrl).pipe(
                map((res: any) => res.entry)
            );
        } else {
            this.logService.error('AppName and TaskId are mandatory for querying a task');
            return throwError('AppName/TaskId not configured');
        }
    }

    /**
     * Creates a new standalone task.
     * @param taskDetails Details of the task to create
     * @returns Details of the newly created task
     */
    createNewTask(startTaskRequest: IgStartTaskRequestModel, appName: string): Observable<IgTaskDetailsModel> {
        const queryUrl = `${this.getBasePath(appName)}/rb/v1/tasks`;
        const payload = JSON.stringify(new IgStartTaskRequestModel(startTaskRequest));

        return this.post<any, IgStartTaskResponseModel>(queryUrl, payload)
            .pipe(
                map(response => response.entry)
            );
    }

    /**
     * Updates the details (name, description, due date) for a task.
     * @param appName Name of the app
     * @param taskId ID of the task to update
     * @param payload Data to update the task
     * @returns Updated task details
     */
    updateTask(appName: string, taskId: string, payload: any): Observable<IgTaskDetailsModel> {
        if ((appName || appName === '') && taskId) {
            payload.payloadType = 'UpdateTaskPayload';
            const queryUrl = `${this.getBasePath(appName)}/rb/v1/tasks/${taskId}`;

            return this.put(queryUrl, payload).pipe(
                map((res: any) => res.entry)
            );
        } else {
            this.logService.error('AppName and TaskId are mandatory for querying a task');
            return throwError('AppName/TaskId not configured');
        }
    }

    /**
     * Gets candidate users of the task.
     * @param appName Name of the app
     * @param taskId ID of the task
     * @returns Candidate users
     */
    getCandidateUsers(appName: string, taskId: string): Observable<string[]> {
        if ((appName || appName === '') && taskId) {
            const queryUrl = `${this.getBasePath(appName)}/query/v1/tasks/${taskId}/candidate-users`;
            return this.get<string[]>(queryUrl);
        } else {
            this.logService.error('AppName and TaskId are mandatory to get candidate user');
            return of([]);
        }
    }

    /**
     * Gets the process definitions associated with an app.
     * @param appName Name of the target app
     * @returns Array of process definitions
     */
    getProcessDefinitions(appName: string): Observable<IgProcessDefinition[]> {
        if (appName || appName === '') {
            const url = `${this.getBasePath(appName)}/rb/v1/process-definitions`;

            return this.get(url).pipe(
                map((res: any) => {
                    return res.list.entries.map((processDefs) => new IgProcessDefinition(processDefs.entry));
                })
            );
        } else {
            this.logService.error('AppName is mandatory for querying task');
            return throwError('AppName not configured');
        }
    }

    /**
     * Updates the task assignee.
     * @param appName Name of the app
     * @param taskId ID of the task to update assignee
     * @param assignee assignee to update current user task assignee
     * @returns Updated task details with new assignee
     */
    assign(appName: string, taskId: string, assignee: string): Observable<IgTaskDetailsModel> {
        if (appName && taskId) {
            const payLoad = { 'assignee': assignee, 'taskId': taskId, 'payloadType': 'AssignTaskPayload' };
            const url = `${this.getBasePath(appName)}/rb/v1/tasks/${taskId}/assign`;

            return this.post(url, payLoad).pipe(
                map((res: any) => {
                    return res.entry;
                })
            );
        } else {
            this.logService.error('AppName and TaskId are mandatory to change/update the task assignee');
            return throwError('AppName/TaskId not configured');
        }
    }

    getPriorityLabel(priority: number): string {
        const priorityItem = this.priorities.find(item => item.value === priority.toString()) || this.priorities[0];
        return this.translateService.instant(priorityItem.label);
    }

    get priorities(): IgTaskPriorityOption[] {
        return this.appConfigService.get('adf-cloud-priority-values') || IG_DEFAULT_TASK_PRIORITIES;
    }

    private isAssignedToMe(assignee: string): boolean {
        const currentUser = this.identityUserService.getCurrentUserInfo().username;
        return assignee === currentUser;
    }
}
