import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '@alfresco/adf-core';

@Component({
    templateUrl: './task-details.component.html',
    styleUrls: ['./task-details.component.css']
})
export class TaskDetailsComponent {

    taskId: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private notificationService: NotificationService
    ) {
        this.route.params.subscribe((params) => {
            this.taskId = params.taskId;
        });
    }

    isTaskValid(): boolean {
        return this.taskId !== undefined;
    }

    goBack() {
        this.router.navigate([`/community/tasks`]);
    }

    onCompletedTask() {
        this.goBack();
    }

    onUnclaimTask() {
        this.goBack();
    }

    onClaimTask() {
        this.goBack();
    }

    onTaskCompleted() {
        this.goBack();
    }

    onFormSaved() {
        this.notificationService.openSnackMessage('Task has been saved successfully');
    }
}
