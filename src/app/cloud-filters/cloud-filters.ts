import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { CloudLayoutService } from '../services/cloud-layout.service';
@Component({
  selector: 'app-cloud-filters',
  templateUrl: './cloud-filters.html',
  styleUrls: ['./cloud-filters.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloudFiltersComponent implements OnInit {

  currentTaskFilter$: Observable<any>;
  currentProcessFilter$: Observable<any>;

  toggleTaskFilter = true;
  toggleProcessFilter = true;

  expandTaskFilter = true;
  expandProcessFilter = false;

  constructor(
      private cloudLayoutService: CloudLayoutService,
      private router: Router,
      private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.currentTaskFilter$ = this.cloudLayoutService.taskFilter$;
    this.currentProcessFilter$ = this.cloudLayoutService.processFilter$;

    let root = '';
    if (this.route.snapshot && this.route.snapshot.firstChild) {
      root = this.route.snapshot.firstChild.url[0].path;
      if (root === 'tasks') {
        this.expandTaskFilter = true;
        this.expandProcessFilter = false;
      } else if (root === 'processes') {
        this.expandProcessFilter = true;
        this.expandTaskFilter = false;
      }
    }
  }

  onTaskFilterSelected(filter) {
    this.cloudLayoutService.setCurrentTaskFilterParam({id: filter.id});
    const currentFilter = Object.assign({}, filter);
    this.router.navigate([`/community/tasks/`], { queryParams: currentFilter });
  }

  onProcessFilterSelected(filter) {
    this.cloudLayoutService.setCurrentProcessFilterParam({id: filter.id});
    const currentFilter = Object.assign({}, filter);
    this.router.navigate([`/cloud/community/processes/`], { queryParams: currentFilter });
  }

  onTaskFilterOpen(): boolean {
    this.expandTaskFilter = true;
    this.expandProcessFilter = false;
    return this.toggleTaskFilter;
  }

  onTaskFilterClose(): boolean {
    return !this.toggleTaskFilter;
  }

  onProcessFilterOpen(): boolean {
    this.expandProcessFilter = true;
    this.expandTaskFilter = false;
    return this.toggleProcessFilter;
  }

  onProcessFilterClose(): boolean {
    return !this.toggleProcessFilter;
  }
}
